import os

from peewee import PostgresqlDatabase, Model, CharField, SmallIntegerField,\
    TextField, DateTimeField, FloatField, IntegerField, CompositeKey


db = PostgresqlDatabase(
    os.getenv("DB_NAME"),
    user=os.getenv("DB_USER"),
    password=os.getenv("DB_PASSWORD"),
    host=os.getenv("DB_HOST"),
    port=int(os.getenv("DB_PORT")))


class BaseModel(Model):
    class Meta:
        database = db


class Meta(BaseModel):
    domain = CharField()
    domain_id = SmallIntegerField()
    name = CharField()
    name_id = SmallIntegerField()
    description = TextField(null=True)
    unit = CharField(null=True)
    dtype = CharField()

    class Meta:
        primary_key = CompositeKey('domain_id', 'name_id')
        indexes = (
            (('domain', 'domain_id', 'name', 'name_id'), True),
        )


class FloatParameter(BaseModel):
    domain_id = SmallIntegerField()
    name_id = SmallIntegerField()
    time = DateTimeField(primary_key=True)
    value = FloatField()

    class Meta:
        indexes = (
            (('domain_id', 'name_id', 'time'), True),
        )


class SmallIntParameter(BaseModel):
    domain_id = SmallIntegerField()
    name_id = SmallIntegerField()
    time = DateTimeField(primary_key=True)
    value = SmallIntegerField()

    class Meta:
        indexes = (
            (('domain_id', 'name_id', 'time'), True),
        )


class IntParameter(BaseModel):
    domain_id = SmallIntegerField()
    name_id = SmallIntegerField()
    time = DateTimeField(primary_key=True)
    value = IntegerField()

    class Meta:
        indexes = (
            (('domain_id', 'name_id', 'time'), True),
        )


tables = [Meta, FloatParameter, SmallIntParameter, IntParameter]
