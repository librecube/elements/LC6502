from datetime import datetime, timedelta
import io
import json
import zipfile

from .models import db, tables, Meta, FloatParameter, SmallIntParameter,\
    IntParameter


def init_db():
    try:
        db.drop_tables(tables)
        db.create_tables(tables)
        for table in tables:
            if table == Meta:
                continue
            name = table._meta.table_name
            db.execute_sql(
                "SELECT create_hypertable('{}', 'time');".format(name))
    except Exception:
        pass


def get_meta(domain, model):
    try:
        query = Meta.select().where(
            Meta.domain == domain,
            Meta.name == model).get()
        domain_id = query.domain_id
        name_id = query.name_id

        if query.dtype == 'float':
            table = FloatParameter
        elif query.dtype == 'smallint':
            table = SmallIntParameter
        elif query.dtype == 'int':
            table = IntParameter
        if None in [table, domain_id, name_id]:
            return None
        return table, query.domain_id, query.name_id

    except Exception:
        return None


def get_domains():
    return [x.domain for x in Meta.select(Meta.domain).distinct()]


def get_models(domain):
    models = [
        x.name for x in Meta.select().where(Meta.domain == domain)]
    return sorted(models)


def get_domain_ids():
    return {
        x.domain: x.domain_id
        for x in Meta.select(Meta.domain, Meta.domain_id).distinct()}


def get_domain_name_ids(domain):
    return {
        x.name: x.name_id
        for x in Meta.select(Meta.name, Meta.name_id)
        .where(Meta.domain == domain)}


def get_new_id(id_dict):
    if not id_dict.values():
        return 0
    return max(id_dict.values()) + 1


def to_isoformat(object):
    if isinstance(object, datetime):
        return object.strftime("%Y-%m-%dT%H:%M:%S.%f")
    else:
        raise NotImplementedError


def from_isoformat(string):
    dt, _, us = string.partition(".")
    dt = datetime.strptime(dt, "%Y-%m-%dT%H:%M:%S")
    us = int(us.rstrip("Z")) if us else 0
    return dt + timedelta(microseconds=us)


def zip_dump(dump):
    mem_file = io.BytesIO()
    with zipfile.ZipFile(mem_file, 'w') as zf:
        handle = zipfile.ZipInfo('dump.json')
        zf.writestr(
            handle, json.dumps(dump, indent=2),
            compress_type=zipfile.ZIP_DEFLATED)
    mem_file.seek(0)
    return mem_file


def import_from_dump(dump):
    number_of_entries = 0

    for domain in dump.keys():
        if domain == "_meta":
            with db.atomic():
                for dataset in dump[domain]:
                    Meta.insert(
                        domain=dataset['domain'],
                        domain_id=dataset['domain_id'],
                        name=dataset['name'],
                        name_id=dataset['name_id'],
                        description=dataset['description'],
                        unit=dataset['unit'],
                        dtype=dataset['dtype'],
                        ).on_conflict_ignore().execute()

        elif domain == "floatparameter":
            with db.atomic():
                for dataset in dump[domain]:
                    FloatParameter.insert(
                        domain_id=dataset['domain_id'],
                        name_id=dataset['name_id'],
                        time=from_isoformat(dataset["time"]),
                        value=float(dataset["value"]),
                        ).on_conflict_ignore().execute()

        elif domain == "intparameter":
            with db.atomic():
                for dataset in dump[domain]:
                    IntParameter.insert(
                        domain_id=dataset['domain_id'],
                        name_id=dataset['name_id'],
                        time=from_isoformat(dataset["time"]),
                        value=int(dataset["value"]),
                        ).on_conflict_ignore().execute()

        elif domain == "smallintparameter":
            with db.atomic():
                for dataset in dump[domain]:
                    SmallIntParameter.insert(
                        domain_id=dataset['domain_id'],
                        name_id=dataset['name_id'],
                        time=from_isoformat(dataset["time"]),
                        value=int(dataset["value"]),
                        ).on_conflict_ignore().execute()

        else:
            raise NotImplementedError
        number_of_entries += len(dump[domain])

    return number_of_entries


def dump_all_domains():
    dump = {}
    datasets = [x for x in FloatParameter.select().dicts()]
    for dataset in datasets:
        dataset["time"] = to_isoformat(dataset["time"])
    dump["floatparameter"] = datasets

    datasets = [x for x in IntParameter.select().dicts()]
    for dataset in datasets:
        dataset["time"] = to_isoformat(dataset["time"])
    dump["intparameter"] = datasets

    datasets = [x for x in SmallIntParameter.select().dicts()]
    for dataset in datasets:
        dataset["time"] = to_isoformat(dataset["time"])
    dump["smallintparameter"] = datasets
    return dump
